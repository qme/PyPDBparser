# Parse PDBs, easily.

- scalable
- easy to use
- minimal



Protein Data Bank Format: 
Coordinate Section
Record Type	Columns	Data	Justification	Data Type
ATOM	1-4	“ATOM”		character
7-11#	Atom serial number	right	integer
13-16	Atom name	left*	character
17	Alternate location indicator		character
18-20§	Residue name	right	character
22	Chain identifier		character
23-26	Residue sequence number	right	integer
27	Code for insertions of residues		character
31-38	X orthogonal Å coordinate	right	real (8.3)
39-46	Y orthogonal Å coordinate	right	real (8.3)
47-54	Z orthogonal Å coordinate	right	real (8.3)
55-60	Occupancy	right	real (6.2)
61-66	Temperature factor	right	real (6.2)
73-76	Segment identifier¶	left	character
77-78	Element symbol	right	character
79-80	Charge		character
HETATM	1-6	“HETATM”		character
7-80	same as ATOM records
TER	1-3	“TER”		character
7-11#	Serial number	right	integer
18-20§	Residue name	right	character
22	Chain identifier		character
23-26	Residue sequence number	right	integer
27	Code for insertions of residues		character
