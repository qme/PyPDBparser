## this is the main parser class.



class parser:

    def __init__(self, pdbfile,save=False,json=True):

        ## initial parameters
        self.filename = pdbfile        
        self.save = save
        self.rows = []
        self.json = json

    def to_string(self,item):
        
        ## return stringified version of an entry
        return "".join([x.replace(" ","") for x in item])
    
    def get_rows(self):
        with open(self.filename) as inpdb:
            for line in inpdb:                
                tokens = list(line)
                row = {}
                
                if "".join(tokens[0:3]) != "TER":
                    
                    if self.to_string(tokens[0:4]) == "ATOM":                        
                        row['ID'] = self.to_string(tokens[0:4])
                        
                    elif self.to_string(tokens[0:6]) == "HETATM":
                        row['ID'] = self.to_string(tokens[0:6])
                        
                    row['atomSerial'] = self.to_string(tokens[6:11])
                    row['atomName'] = self.to_string(tokens[12:16])
                    row['altLoc'] = self.to_string(tokens[16])
                    row['resName'] = self.to_string(tokens[17:20])
                    row['chainID'] = self.to_string(tokens[21])
                    row['resSeqNR'] = self.to_string(tokens[22:26])
                    row['insertion'] = self.to_string(tokens[26])
                    row['x'] = self.to_string(tokens[30:38])
                    row['y'] = self.to_string(tokens[39:46])
                    row['z'] = self.to_string(tokens[47:54])
                    row['occupacy'] = self.to_string(tokens[54:60])
                    row['tempFac'] = self.to_string(tokens[60:66])
                    row['segID'] = self.to_string(tokens[72:76])
                    row['elSym'] = self.to_string(tokens[76:78])
                    row['charge'] = self.to_string(tokens[78:80])
                    
                elif "".join(tokens[0:3]) == "TER":
                    
                    row['charge'] = self.to_string(tokens[78:80])
                    
                else:
                    pass
                
                if self.save:
                    self.rows.append(row)
                    
                elif self.json:
                    yield row
                    
                elif self.json == False:
                    yield list(row.values())
                    
                else:
                    pass
    pass

if __name__ == '__main__':

    ## some basic tests here..    
    newParser = parser("test.pdb")
    for row in newParser.get_rows():
        print(row)
